# GohostShips

Welcome!  Your aim is to help eliminate the enemy by upgrading our automated battleship AI's.

Game Rules:
 - There are 2 teams
 - The first team to have all their ships destroyed loses
 - The winning team will receive a point for every remaining ship after the enemy is destroyed.
 - The game will last a maximum of 6 minutes, after this time has elapsed teams get scored on remaining vessels.

Movement Rules:
 - Ships may move in any given direction each turn, including no movement at all
 - Any movement of both ships and torpedoes is limited to up, down, left, right; no diagonal movement is possible
 - Ships move at a fixed speed
 - Torpedoes move at a fixed speed
 - Ships cannot turn, yet can move in any given direction

Collision Rules:
 - Ships must not collide with the shore, or they will be shipwrecked
 - Ships, colliding with other ships, will both be destroyed regardless of if they're friendly or foe
 - Torpedoes do not collide with another
 - Torpedoes collide with both friendly and enemy ships
 - All torpedoes move before ships on each turn

AI Notes:
 - Be aware that Torpedoes can only be fired every 100 triggers of the ships AI - be sure to check the weapons cooldown before attempting to fire.
 - All ship & torpedo coordinates are given from the top-left corner of the object.  Be very careful to keep length and width of both ships and torpedoes in mind.
 - Each time the AI is triggered, actions are performed in the following order:
    - The AI is provided an intelligence update, this will include data on 1 ship on the sea and any active torpedoes
    - The AI is granted access to the team radio to where it may listen and submit messages to team vessels
    - The AI is asked to provide a command.  The command includes a movement direction and fire direction (remember there is a cooldown rate on firing)
    

