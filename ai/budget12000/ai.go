package budget12000

import (
	"eng-games/gohostships"
	"math/rand"
	"time"
)

type Ai struct {
	mapX, mapY int

	x, y int
	currentDirection gohostships.Direction
	readyToFire bool

	ships map[string]ship
	torpedos map[string]torpedo
}

type ship struct {
	x, y int
}

type torpedo struct {
	x, y int
	direction gohostships.Direction
}

func New() *Ai{
	rand.Seed(time.Now().UnixNano())
	return &Ai{}
}

func (ai *Ai) Init(mapX, mapY int) gohostships.Ai {
	ai.mapX, ai.mapY = mapX, mapY
	ai.readyToFire = false
	ai.changeDirection()
	return ai
}

func (ai *Ai) IntelligenceUpdate(br gohostships.BattleReport) {
	ai.x = int(br.CurrentLocation.X)
	ai.y = int(br.CurrentLocation.Y)

	if br.WeaponsCooldown == 0 {
		ai.readyToFire = true
	} else {
		ai.readyToFire = false
	}

	if br.ShipReport.Status == "" {
		return
	}
}

func (ai *Ai) Command() gohostships.Command {
	for {
		if ai.currentDirection == gohostships.Left {
			if ai.x <= gohostships.ShipSpeed {
				ai.changeDirection()
				continue
			}
		} else if ai.currentDirection == gohostships.Right {
			if ai.x >= ai.mapX - gohostships.ShipSpeed - gohostships.ShipWidth {
				ai.changeDirection()
				continue
			}
		} else if ai.currentDirection == gohostships.Down {
			if ai.y >= ai.mapY - gohostships.ShipSpeed - gohostships.ShipHeight {
				ai.changeDirection()
				continue
			}
		} else if ai.currentDirection == gohostships.Up {
			if ai.y <= gohostships.ShipSpeed {
				ai.changeDirection()
				continue
			}
		}
		break
	}

	c :=  gohostships.Command{
		Move: ai.currentDirection,
	}

	if ai.readyToFire {
		c.Fire = ai.currentDirection
	}

	return c
}

func (ai *Ai) changeDirection() {
	directions := []gohostships.Direction{gohostships.Left, gohostships.Right, gohostships.Up, gohostships.Down}
	ai.currentDirection = directions[rand.Intn(len(directions))]
}

func (ai *Ai) Communicate(r *gohostships.Radio) {

}

