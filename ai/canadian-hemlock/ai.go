package canadian_hemlock

import (
	"eng-games/gohostships"
	"fmt"
	"math"
	"math/rand"
)

// type ShipBounds struct{

// }
type Ai struct {
	mapX, mapY int

	x, y int

	currentDirection gohostships.Direction
	readyToFire      bool

	ships     map[string]gohostships.ShipReport
	torpedeos map[string]gohostships.Torpedo
}

func New() *Ai {
	return &Ai{}
}

func (ai *Ai) Init(mapX, mapY int) gohostships.Ai {
	ai.mapX, ai.mapY = mapX, mapY
	ai.ships = make(map[string]gohostships.ShipReport)
	ai.torpedeos = make(map[string]gohostships.Torpedo)
	ai.currentDirection = randomDirection()
	return ai
}

func (ai *Ai) IntelligenceUpdate(br gohostships.BattleReport) {
	ai.x = int(br.CurrentLocation.X)
	ai.y = int(br.CurrentLocation.Y)

	/* we always want to know when we're ready to fire */
	if br.WeaponsCooldown == 0 {
		ai.readyToFire = true
	} else {
		ai.readyToFire = false
	}

	//fmt.Printf("%+v\n", br)

	//if (br.ShipReport.Team !=  ai.T)

	ai.ships[br.ShipReport.Name] = br.ShipReport
	for _, t := range br.ShipReport.Torpedoes {
		ai.torpedeos[t.ID] = t
	}
}

func (ai *Ai) Command() gohostships.Command {
	possibleDirections := make(map[gohostships.Direction]bool)
	possibleDirections[gohostships.Left] = true
	possibleDirections[gohostships.Right] = true
	possibleDirections[gohostships.Up] = true
	possibleDirections[gohostships.Down] = true

	if ai.x <= gohostships.ShipSpeed {
		possibleDirections[gohostships.Left] = false
	}
	if ai.x+gohostships.ShipWidth+gohostships.ShipSpeed >= ai.mapX {
		possibleDirections[gohostships.Right] = false
	}
	if ai.y+gohostships.ShipHeight+gohostships.ShipSpeed >= ai.mapY {
		possibleDirections[gohostships.Down] = false
	}
	if ai.y <= gohostships.ShipSpeed {
		possibleDirections[gohostships.Up] = false
	}

	fmt.Printf("Ship Located at: %d, %d\n", ai.x, ai.y)

	// torpedo detection
	for _, t := range ai.torpedeos {
		fmt.Printf("… torpedo located at: %f, %f\n", t.X, t.Y)
		diffX := math.Abs(float64(ai.x) - t.X)
		diffY := math.Abs(float64(ai.y) - t.Y)

		incoming_detected := false

		// Coming from left towards ship
		if t.X < float64(ai.x)+200 && t.Direction == gohostships.Left && diffY <= 13 {
			possibleDirections[gohostships.Left] = false
			possibleDirections[gohostships.Right] = false
			incoming_detected = true
		}
		// Coming from
		if t.X > float64(ai.x)-200 && t.Direction == gohostships.Right && diffY <= 13 {
			possibleDirections[gohostships.Left] = false
			possibleDirections[gohostships.Right] = false
			incoming_detected = true
		}
		if t.Y > float64(ai.y)-200 && t.Direction == gohostships.Down && diffX <= 100 {
			possibleDirections[gohostships.Up] = false
			possibleDirections[gohostships.Down] = false
			incoming_detected = true
		}
		if t.Y < float64(ai.y)+200 && t.Direction == gohostships.Up && diffX <= 100 {
			possibleDirections[gohostships.Up] = false
			possibleDirections[gohostships.Down] = false
			incoming_detected = true
		}
		fmt.Printf("Incoming torpedo detected: %+v\n", incoming_detected)
	}

	if !possibleDirections[ai.currentDirection]{
		ai.currentDirection = ai.changeDirection(possibleDirections)
	}

	c := gohostships.Command{
		Move: ai.currentDirection,
	}

	if ai.readyToFire {
		// fire and hope for the best!
		c.Fire = randomDirection()
	}

	return c

}

func (ai *Ai) changeDirection(possibleDirections map[gohostships.Direction]bool) gohostships.Direction {
	if possibleDirections[gohostships.Left] {
		return gohostships.Left
	}

	if possibleDirections[gohostships.Right] {
		return gohostships.Right
	}

	if possibleDirections[gohostships.Up] {
		return gohostships.Up
	}

	if possibleDirections[gohostships.Down] {
		return gohostships.Down
	}

	return ""
}

func (ai *Ai) Communicate(_ *gohostships.Radio) {

}

func randomDirection() gohostships.Direction {
	directions := []gohostships.Direction{gohostships.Left, gohostships.Right, gohostships.Up, gohostships.Down}
	randomDirection := directions[rand.Intn(len(directions))]
	return randomDirection
}
