package bristlecone_pine

import (
	"eng-games/gohostships"
	"math/rand"
)

type Ai struct {
	mapX, mapY int

	x, y             int
	currentDirection gohostships.Direction
	readyToFire      bool

	ships    map[string]ship
	torpedos map[string]torpedo
}

type ship struct {
	x, y int
}

type torpedo struct {
	x, y      int
	direction gohostships.Direction
}

func New() *Ai {
	return &Ai{}
}

func (ai *Ai) Init(mapX, mapY int) gohostships.Ai {
	ai.mapX, ai.mapY = mapX, mapY
	ai.readyToFire = true
	ai.changeDirection()
	return ai
}

func (ai *Ai) IntelligenceUpdate(br gohostships.BattleReport) {
	ai.x = int(br.CurrentLocation.X)
	ai.y = int(br.CurrentLocation.Y)

	if br.WeaponsCooldown == 0 {
		ai.readyToFire = true
	} else {
		ai.readyToFire = false
	}

	if br.ShipReport.Status == "" {
		// ship.x=br.ShipReport.Location.X
		// ship.y=br.ShipReport.Location.Y
		return
	}
}

func (ai *Ai) Command() gohostships.Command {
	for {
		if ai.currentDirection == gohostships.Left {
			if ai.x <= gohostships.ShipSpeed {
				ai.changeDirection()
				continue
			}
		} else if ai.currentDirection == gohostships.Right {
			if ai.x >= ai.mapX-gohostships.ShipSpeed-gohostships.ShipWidth {
				ai.changeDirection()
				continue
			}
		} else if ai.currentDirection == gohostships.Down {
			if ai.y >= ai.mapY-gohostships.ShipSpeed-gohostships.ShipHeight {
				ai.changeDirection()
				continue
			}
		} else if ai.currentDirection == gohostships.Up {
			if ai.y <= gohostships.ShipSpeed {
				ai.changeDirection()
				continue
			}
		}
		break
	}

	// continue moving in this direction
	c := gohostships.Command{
		Move: ai.currentDirection,
	}

	// If you can fire, do so in the same direction
	if ai.readyToFire {
		c.Fire = ai.currentDirection
	}

	return c
}

func (ai *Ai) changeDirection() {
	directions := []gohostships.Direction{gohostships.Left, gohostships.Right, gohostships.Up, gohostships.Down}
	ai.currentDirection = directions[rand.Intn(len(directions))]
	// stop the crash in to the wall...
}

func (ai *Ai) Communicate(r *gohostships.Radio) {
	// received = r.Receive //Array of messages
	// transmit = r.Transmit(fmt.Sprintf("%d, %d, %s", x, y, ai.currentDirection))
}