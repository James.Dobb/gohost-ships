package main

import (
	bristlecone_pine "eng-games/ai/bristlecone-pine"
	canadian_hemlock "eng-games/ai/canadian-hemlock"
	"eng-games/gohostships"
)

const (
	x = 2800
	y = 1400
)

func main() {
	teams := []*gohostships.Team{
		{
			Name:  "Canadian Hemlock",
			Color: gohostships.Red,
			Ships: []*gohostships.Ship{
				gohostships.NewShip("Omar", canadian_hemlock.New().Init(x,y), 899, 98),
				gohostships.NewShip("Chisel", canadian_hemlock.New().Init(x,y), 12, 986),
				gohostships.NewShip("James", canadian_hemlock.New().Init(x,y), 1200, 512),
			},
			Radio: gohostships.NewRadio(),
		},
		{
			Name:  "Bristlecone Pine",
			Color: gohostships.Green,
			Ships: []*gohostships.Ship{
				gohostships.NewShip("Taylor", bristlecone_pine.New().Init(x,y), 500, 1200),
				gohostships.NewShip("Elroy", bristlecone_pine.New().Init(x,y), 500, 1000),
				gohostships.NewShip("Wilson", bristlecone_pine.New().Init(x,y), 657, 897),
				gohostships.NewShip("Khanh", bristlecone_pine.New().Init(x,y), 2401, 1100),
			},
			Radio: gohostships.NewRadio(),
		},
	}

	g := gohostships.New(x, y, teams)
	play(g)
}

func play(g *gohostships.Game)  {
	gohostships.NewGameWindow(g, x, y)
}
