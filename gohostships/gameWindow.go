package gohostships

import (
	"fmt"
	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/ebitenutil"
	"image/color"
)

const bankSize = 50

type GameWindow struct {
	x int
	y int
	game *Game
}

func NewGameWindow(game *Game, x int, y int) *GameWindow {
	ebiten.SetWindowSize(x + bankSize*2, y + bankSize*2)
	ebiten.SetWindowTitle("GohostShips")

	gw := &GameWindow{game: game, x:x, y:y}

	ebiten.SetMaxTPS(30) // Game speed FPS

	err := ebiten.RunGame(gw)
	if err != nil {
		fmt.Println(err)
	}

	return gw
}

var shipRed, _, _ = ebitenutil.NewImageFromFile("./img/ship-red.png", 0)
var shipGreen, _, _ = ebitenutil.NewImageFromFile("./img/ship-green.png", 0)

var torpedoLeft, _, _ = ebitenutil.NewImageFromFile("./img/torpedo-left.png", 0)
var torpedoRight, _, _ = ebitenutil.NewImageFromFile("./img/torpedo-right.png", 0)
var torpedoUp, _, _ = ebitenutil.NewImageFromFile("./img/torpedo-up.png", 0)
var torpedoDown, _, _ = ebitenutil.NewImageFromFile("./img/torpedo-down.png", 0)

var explosion, _, _ = ebitenutil.NewImageFromFile("./img/explosion.png", 0)
var aiCrash, _, _ = ebitenutil.NewImageFromFile("./img/ai-crash.png", 0)

func (g *GameWindow) Update(screen *ebiten.Image) error {
	g.game.Tick()

	_ = screen.Fill(color.RGBA{
		R: 192,
		G: 204,
		B: 126,
		A: 255,
	})

	sea, _ := ebiten.NewImage(g.x, g.y, 0)
	_ = sea.Fill(color.RGBA{
		R: 139,
		G: 192,
		B: 245,
		A: 255,
	})
	op := &ebiten.DrawImageOptions{}
	op.GeoM.Translate(bankSize, bankSize)
	_ = screen.DrawImage(sea, op)

	for _, s := range g.game.Ships {
		op := &ebiten.DrawImageOptions{}
		op.GeoM.Translate(s.x + bankSize, s.y + bankSize)
		if s.team.Color == Red {
			_ = screen.DrawImage(shipRed, op)
		} else {
			_ = screen.DrawImage(shipGreen, op)
		}
		ebitenutil.DebugPrintAt(screen, s.name, int(s.x + bankSize + 12), int(s.y + bankSize + 34))

		if s.status != Healthy {
			op := &ebiten.DrawImageOptions{}
			op.GeoM.Translate(s.x + bankSize, s.y + bankSize)

			if s.status == ComputerSaysNo {
				_ = screen.DrawImage(aiCrash, op)
			} else {
				_ = screen.DrawImage(explosion, op)
			}
		}

		for _, t := range s.torpedoes {
			op := &ebiten.DrawImageOptions{}
			op.GeoM.Translate(t.X + bankSize, t.Y + bankSize)

			if t.Direction == Left {
				_ = screen.DrawImage(torpedoLeft, op)
			} else if t.Direction == Right {
				_ = screen.DrawImage(torpedoRight, op)
			} else if t.Direction == Up {
				_ = screen.DrawImage(torpedoUp, op)
			} else if t.Direction == Down {
				_ = screen.DrawImage(torpedoDown, op)
			}
		}
	}

	return nil
}

func (g *GameWindow) Layout(outsideWidth, outsideHeight int) (screenWidth, screenHeight int) {
	return outsideWidth, outsideHeight
}
