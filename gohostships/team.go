package gohostships

type TeamColour string

const (
	Red TeamColour = "red"
	Green TeamColour = "green"
)

type Team struct {
	Name  string
	Color TeamColour
	Ships []*Ship
	Radio *Radio
}

func (t *Team) HealthyShips() int {
	hs := 0

	for _, s := range t.Ships {
		if s.status == Healthy {
			hs++
		}
	}

	return hs
}