package gohostships

type Direction string

const (
	Still Direction = ""
	Up Direction = "up"
	Down Direction = "down"
	Left Direction = "left"
	Right Direction = "right"

	TorpedoLength = 45
	TorpedoWidth = 13
	TorpedoSpeed = 5
)

type Torpedo struct {
	ID string
	X float64
	Y float64
	Direction Direction
}


