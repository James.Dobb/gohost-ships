package gohostships

type BattleReport struct {
	CurrentLocation Location
	WeaponsCooldown int
	ShipReport      ShipReport
}

type Location struct {
	X float64
	Y float64
}

type ShipReport struct {
	Location  Location
	Name      string
	Team      string
	Status    ShipStatus
	Torpedoes []Torpedo
}
