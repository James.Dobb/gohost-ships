package gohostships

import "github.com/google/uuid"

type ShipStatus string

const (
	Healthy        ShipStatus = "healthy"
	Collided       ShipStatus = "collided"
	Destroyed      ShipStatus = "destroyed"
	Shipwrecked    ShipStatus = "shipwrecked"
	ComputerSaysNo ShipStatus = "computerSaysNo"

	ShipWidth  = 128
	ShipHeight = 50

	ShipSpeed        = 3
	ShipFireCooldown = 100
)

type Ship struct {
	name         string
	team         *Team
	ai           Ai
	x            float64
	y            float64
	status       ShipStatus
	torpedoes    []*Torpedo
	fireCooldown int
}

func NewShip(name string, ai Ai, startX float64, startY float64) *Ship {
	return &Ship{
		name:         name,
		team:         nil,
		ai:           ai,
		x:            startX,
		y:            startY,
		status:       Healthy,
		torpedoes:    []*Torpedo{},
		fireCooldown: ShipFireCooldown,
	}
}

func (s *Ship) BoundingBox() (float64, float64, float64, float64) {
	return s.x, s.x + ShipWidth, s.y, s.y + ShipHeight
}

func (s *Ship) Turn(br BattleReport) {
	defer func() {
		if r := recover(); r != nil {
			s.status = ComputerSaysNo
			return
		}
	}()

	br.CurrentLocation = Location{X: s.x, Y: s.y}
	br.WeaponsCooldown = s.fireCooldown
	s.ai.IntelligenceUpdate(br)

	s.ai.Communicate(s.team.Radio)

	c := s.ai.Command()

	if c.Move != "" {
		if c.Move == Up {
			s.y -= ShipSpeed
		} else if c.Move == Down {
			s.y += ShipSpeed
		} else if c.Move == Left {
			s.x -= ShipSpeed
		} else if c.Move == Right {
			s.x += ShipSpeed
		}
	}

	if c.Fire != "" && s.fireCooldown <= 0 {
		x := s.x
		y := s.y

		if c.Fire == Up {
			y -= ShipHeight
			x += ShipWidth / 2
		} else if c.Fire == Down {
			y += ShipHeight
			x += ShipWidth / 2
		} else if c.Fire == Left {
			y += ShipHeight / 2
			x -= ShipWidth
		} else if c.Fire == Right {
			y += ShipHeight / 2
			x += ShipWidth
		}

		s.torpedoes = append(s.torpedoes, &Torpedo{
			ID:        uuid.NewString(),
			X:         x,
			Y:         y,
			Direction: c.Fire,
		})

		s.fireCooldown = ShipFireCooldown
	}

	s.fireCooldown--
	if s.fireCooldown < 0 {
		s.fireCooldown = 0
	}
}
