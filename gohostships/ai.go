package gohostships

type Ai interface {
	Init(mapX, mapY int) Ai

	IntelligenceUpdate(report BattleReport)

	Command() Command

	Communicate(r *Radio)
}