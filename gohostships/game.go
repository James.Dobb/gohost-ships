package gohostships

import (
	"math"
	"math/rand"
	"time"
)

type Game struct {
	X     float64
	Y     float64
	Teams []*Team
	Ships []*Ship
}

func New(x float64, y float64, teams []*Team) *Game {
	rand.Seed(time.Now().Unix())

	g := &Game{
		X:     x,
		Y:     y,
		Teams: teams,
		Ships: []*Ship{},
	}

	for _, t := range g.Teams {
		for _, s := range t.Ships {
			s.team = t
			g.Ships = append(g.Ships, s)
		}
	}

	return g
}

func (g *Game) Tick() bool {
	for _, t := range g.Teams {
		if t.HealthyShips() == 0 {
			return false
		}
	}

	// Torpedoes move
	for _, s := range g.Ships {
		for ti := len(s.torpedoes) - 1; ti >= 0; ti-- {
			g.TorpedoIt(s, s.torpedoes[ti], ti)
		}
	}

	// Ships move
	for _, s := range g.Ships {
		g.ShipIt(s)
	}

	return true
}

func (g *Game) TorpedoIt(s *Ship, t *Torpedo, ti int) {
	if t.Direction == Up {
		t.Y -= TorpedoSpeed
	} else if t.Direction == Down {
		t.Y += TorpedoSpeed
	} else if t.Direction == Left {
		t.X -= TorpedoSpeed
	} else if t.Direction == Right {
		t.X += TorpedoSpeed
	}

	if t.Direction == Left || t.Direction == Right {
		if t.X < 0 || t.X >= g.X-TorpedoLength {
			s.torpedoes = RemoveIndex(s.torpedoes, ti)
			return
		}
	} else {
		if t.Y < 0 || t.Y >= g.Y-TorpedoLength {
			s.torpedoes = RemoveIndex(s.torpedoes, ti)
			return
		}
	}

	for _, sc := range g.Ships {
		intersect := false
		if t.Direction == Left || t.Direction == Right {
			intersect = DoBoxesIntersect(sc.x, sc.y, t.X, t.Y, ShipWidth, ShipHeight, TorpedoLength, TorpedoWidth)
		} else {
			intersect = DoBoxesIntersect(sc.x, sc.y, t.X, t.Y, ShipWidth, ShipHeight, TorpedoWidth, TorpedoLength)
		}

		if intersect {
			sc.status = Destroyed
			s.torpedoes = RemoveIndex(s.torpedoes, ti)
			return
		}
	}
}

func (g *Game) ShipIt(s *Ship) {
	if s.status != Healthy {
		return
	}

	s.Turn(g.PrepareReport())

	if g.IsShipWrecked(s) {
		s.status = Shipwrecked
		return
	}

	if g.HasCollided(s) {
		s.status = Collided
		return
	}
}

func (g *Game) PrepareReport() BattleReport {
	br := BattleReport{
		ShipReport: ShipReport{},
	}

	r := rand.Intn(len(g.Ships) - 1)

	sr := ShipReport{
		Location: Location{
			X: g.Ships[r].x,
			Y: g.Ships[r].y,
		},
		Name:      g.Ships[r].name,
		Team:      g.Ships[r].team.Name,
		Status:    g.Ships[r].status,
		Torpedoes: []Torpedo{},
	}

	for _, t := range g.Ships[r].torpedoes {
		sr.Torpedoes = append(sr.Torpedoes, *t)
	}

	br.ShipReport = sr

	return br
}

func (g *Game) IsShipWrecked(s *Ship) bool {
	l, r, t, b := s.BoundingBox()

	if l < 0 || r > g.X {
		return true
	}

	if t < 0 || b > g.Y {
		return true
	}

	return false
}

func (g *Game) HasCollided(s *Ship) bool {
	for _, cs := range g.Ships {
		if cs == s {
			continue
		}

		if DoBoxesIntersect(s.x, s.y, cs.x, cs.y, ShipWidth, ShipHeight, ShipWidth, ShipHeight) {
			s.status = Collided
			cs.status = Collided
			return true
		}

		for k, t := range cs.torpedoes {
			intersect := false
			if t.Direction == Left || t.Direction == Right {
				intersect = DoBoxesIntersect(s.x, s.y, t.X, t.Y, ShipWidth, ShipHeight, TorpedoLength, TorpedoWidth)
			} else {
				intersect = DoBoxesIntersect(s.x, s.y, t.X, t.Y, ShipWidth, ShipHeight, TorpedoWidth, TorpedoLength)
			}

			if intersect {
				s.status = Destroyed
				cs.torpedoes = RemoveIndex(cs.torpedoes, k)
				return true
			}
		}
	}

	return false
}

func DoBoxesIntersect(aX, aY, bX, bY, aW, aH, bW, bH float64) bool {
	return (math.Abs((aX+aW/2)-(bX+bW/2))*2 < (aW + bW)) &&
		(math.Abs((aY+aH/2)-(bY+bH/2))*2 < (aH + bH))
}

func RemoveIndex(s []*Torpedo, index int) []*Torpedo {
	return append(s[:index], s[index+1:]...)
}
