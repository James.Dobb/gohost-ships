package gohostships

import "time"

type Transmission struct {
	Time    time.Time
	Message string
}

type Radio struct {
	transmissions []Transmission
}

func NewRadio() *Radio {
	return &Radio{transmissions: []Transmission{}}
}

func (r *Radio) Transmit(msg string) {
	r.transmissions = append(r.transmissions, Transmission{
		Time:    time.Now(),
		Message: msg,
	})
}

func (r *Radio) TotalMessages() int {
	return len(r.transmissions)
}

func (r *Radio) Receive(i int) Transmission {
	if len(r.transmissions) < i-1 {
		return Transmission{
			Time:    time.Now(),
			Message: "",
		}
	}
	return r.transmissions[i]
}
